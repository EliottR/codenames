#!/bin/bash

cd client/
npm i
rm -r build/
npm run build
rm -rf /var/www/codenames/client/*
cp -r build/* /var/www/codenames/client/

echo "Script executed !"