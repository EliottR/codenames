import express from "express";
import { createServer } from "http";
import { Server } from "socket.io";
import cors from "cors";
import sequelize from "./utils/database.mjs";
import Router from "./routes/index.routes.mjs";
import dotenv from "dotenv";
dotenv.config({ path: ".env.local" });

const app = express();
app.use(express.json());
app.use(
  cors({
    credentials: true,
    origin: [process.env.CLIENT_URL, process.env.IP_ADDRESS],
  })
);
const httpServer = createServer(app);

const io = new Server(httpServer, {
  cors: {
    origin: [process.env.CLIENT_URL, process.env.IP_ADDRESS],
    methods: ["GET", "POST"],
  },
});

io.on("connection", (socket, res) => {
  console.log(`User ${socket.id} is connected`);

  socket.on("disconnect", () => {
    console.log(`User ${socket.id} disconnected`);
  });

  socket.on("joinParty", (room) => {
    socket.join(room);
    console.log(`User ${socket.id} joined room ${room} ✅`);
  });

  socket.on("updateDataParty", (room) => {
    socket.to(room).emit("updateDataParty");
  });
});

httpServer.listen(process.env.PORT, () => {
  console.log(`Listening on ${process.env.PORT} 🚀`);
});

app.use("/api", Router);

const connectDb = async () => {
  try {
    await sequelize.authenticate();
    await sequelize.sync({ alter: true });
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
};

connectDb();
