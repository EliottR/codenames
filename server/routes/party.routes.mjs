import express from "express";
import { createParty, deleteParty, getParty } from "../controllers/party.mjs";

const PartyRouter = express.Router();

PartyRouter.post("/create", createParty);

PartyRouter.post("/delete", deleteParty);

PartyRouter.post("/get", getParty);

export default PartyRouter;
