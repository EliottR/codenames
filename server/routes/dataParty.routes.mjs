import express from "express";
import { saveData, updateData } from "../controllers/dataParty.mjs";

const DataPartyRouter = express.Router();

DataPartyRouter.post("/save", saveData);
DataPartyRouter.post("/update", updateData);

export default DataPartyRouter;
