import express from "express";
import { decodeJWT, generateJwt } from "../controllers/index.mjs";
import DataPartyRouter from "./dataParty.routes.mjs";
import PartyRouter from "./party.routes.mjs";
import UserRouter from "./user.routes.mjs";

const Router = express.Router();

Router.post("/jwt/generate", generateJwt);
Router.post("/jwt/decode", decodeJWT);

Router.use("/party", PartyRouter);
Router.use("/dataParty", DataPartyRouter);
Router.use("/users", UserRouter);

Router.get("/test", (_, res) => {
  res.status(200).send("Successful connection");
});

export default Router;
