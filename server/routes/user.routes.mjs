import express from "express";
import { addUser, deleteUser } from "../controllers/user.mjs";

const UserRouter = express.Router();

UserRouter.post("/add", addUser);

UserRouter.post("/delete", deleteUser);

export default UserRouter;
