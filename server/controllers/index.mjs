import jwt from "jsonwebtoken";

export const generateJwt = (req, res) => {
  const { PartyId, username, team, spy } = req.body;

  const token = jwt.sign(
    { PartyId: PartyId, username: username, team: team, spy: spy },
    process.env.SECRET_JWT
  );
  res.status(201).json({ token: token });

  try {
  } catch (err) {
    console.log("error", err);
    res.sendStatus(500);
  }
};

export const decodeJWT = (req, res) => {
  const { token } = req.body;

  const decoded = jwt.verify(token, process.env.SECRET_JWT);

  console.log(decoded);

  if (decoded) {
    res.status(200).json(decoded);
  } else {
    res.status(403).json("Invalid token");
  }
};
