import { DataParty } from "../models/index.mjs";

export const saveData = async (req, res) => {
  const { PartyId, cards, begin } = req.body;

  try {
    await DataParty.create({
      PartyId: PartyId,
      cards: cards,
      begin: begin,
      turn: begin,
    });
    res
      .status(201)
      .json({ PartyId: PartyId, cards: cards, begin: begin, turn: begin });
  } catch (err) {
    console.log("error", err);
    res.sendStatus(500);
  }
};

export const updateData = async (req, res) => {
  const { PartyId, cards, turn, whoWins } = req.body;

  try {
    await DataParty.update(
      {
        cards: cards,
        turn: turn,
        whoWins: whoWins,
      },
      {
        where: {
          PartyId: PartyId,
        },
      }
    );
    res.status(200).json({ PartyId: PartyId, cards: cards, turn: turn });
  } catch (err) {
    console.log("error", err);
    res.sendStatus(500);
  }
};
