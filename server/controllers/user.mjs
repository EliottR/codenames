import { User } from "../models/index.mjs";

export const addUser = async (req, res) => {
  const { PartyId, username, spy, team } = req.body;

  try {
    await User.create({
      PartyId: PartyId,
      username: username,
      spy: spy,
      team: team,
    });
    res.status(201).json({
      PartyId: PartyId,
      username: username,
      spy: spy,
      team: team,
    });
  } catch (err) {
    console.log("error", err);
    res.sendStatus(500);
  }
};

export const deleteUser = async (req, res) => {
  const { PartyId, username } = req.body;

  try {
    await User.destroy({
      where: {
        PartyId: PartyId,
        username: username,
      },
    });
    res.status(200).json({ PartyId: PartyId, username: username });
  } catch (err) {
    console.log("error", err);
    res.sendStatus(500);
  }
};
