import { Party } from "../models/index.mjs";

export const createParty = async (req, res) => {
  const { id } = req.body;

  try {
    await Party.create({ id: id });
    res.status(201).json({ id: id });
  } catch (err) {
    console.log("error", err);
    res.sendStatus(500);
  }
};

export const getParty = async (req, res) => {
  const { id } = req.body;

  try {
    const party = await Party.findByPk(id);
    if (party) {
      const dataParty = await party.getDataParty();
      const users = await party.getUsers();
      res.status(200).json({ dataParty: dataParty, users: users });
    } else {
      res.status(404).json("Room not found");
    }
  } catch (err) {
    // console.log("error", err);
    res.sendStatus(404);
  }
};

export const deleteParty = async (req, res) => {
  const { id } = req.body;

  try {
    await Party.destroy({
      where: {
        id: id,
      },
    });
    res.status(200).json({ id: id });
  } catch (err) {
    console.log("error", err);
    res.sendStatus(500);
  }
};
