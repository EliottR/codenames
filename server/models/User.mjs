import { DataTypes } from "sequelize";
import sequelize from "../utils/database.mjs";

const User = sequelize.define(
  "User",
  {
    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    spy: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    team: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    // Other model options go here
  }
);

export default User;
