import { DataTypes } from "sequelize";
import sequelize from "../utils/database.mjs";

const Party = sequelize.define(
  "Party",
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
    },
  },
  {
    // Other model options go here
  }
);

export default Party;
