import Party from "./Party.mjs";
import DataParty from "./DataParty.mjs";
import User from "./User.mjs";

Party.hasOne(DataParty, { onDelete: "CASCADE" });
Party.hasMany(User, { onDelete: "CASCADE" });
DataParty.belongsTo(Party);
User.belongsTo(Party);

export { Party, DataParty, User };
