import { DataTypes } from "sequelize";
import sequelize from "../utils/database.mjs";

const DataParty = sequelize.define(
  "DataParty",
  {
    cards: {
      type: DataTypes.STRING(5000),
      allowNull: false,
    },
    begin: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    turn: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    whoWins: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  },
  {
    // Other model options go here
  }
);

export default DataParty;
