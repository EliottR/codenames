import { Sequelize } from "sequelize";

const sequelize = new Sequelize("codenames", "postgres", "postgres", {
  host: "localhost",
  dialect: "postgres",
});

export default sequelize;
