const FrontEndPoints = {
  home: "/",
  rules: "rules",
  game: "game",
  gameId: "game/:id",
};

export default FrontEndPoints