const party = 'party/'
const dataParty = 'dataParty/'
const user = 'users/'
const jwt = 'jwt/'

const BackEndPoints = {
  joinParty: party + "create",
  deleteParty: party + "delete",
  getParty: party + "get",
  saveDataParty: dataParty + "save",
  updateDataParty: dataParty + "update",
  addUser: user + "add",
  deleteUser: user + "delete",
  generateJwt: jwt + "generate",
  decodeJwt: jwt + "decode",
};

export default BackEndPoints