import { Navigate, Route, Routes } from "react-router-dom";
import { Game } from "../pages/Game";
import { Home } from "../pages/Home";
import { Rules } from "../pages/Rules";
import FrontEndPoints from "./FrontEndPoints";

export const Router = () => {
  return (
    <Routes>
      <Route path={FrontEndPoints.home} element={<Home />} />
      <Route path={FrontEndPoints.rules} element={<Rules />} />
      <Route
        path={FrontEndPoints.game}
        element={<Navigate to={FrontEndPoints.home} replace />}
      />
      <Route path={FrontEndPoints.game + "/:id"} element={<Game />} />
    </Routes>
  );
};
