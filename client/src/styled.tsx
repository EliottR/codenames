import { createGlobalStyle, ThemeProps } from "styled-components";
import { ThemeProvider } from "styled-components";
import { ReactNode } from "react";

export type ThemeType = {
  backgroundColor: string;
  color: string;
};

interface Props {
  theme: boolean;
  children: ReactNode;
}

export const lightTheme: ThemeType = {
  backgroundColor: "white",
  color: "black",
};

export const darkTheme: ThemeType = {
  color: "white",
  backgroundColor: "black",
};

const Theme = ({ children, theme }: Props) => (
  <ThemeProvider theme={theme ? lightTheme : darkTheme}>
    {children}
  </ThemeProvider>
);

export default Theme;

/* Global style */
export const GlobalStyle = createGlobalStyle`
*{
  margin: 0;
  padding: 0;
}

body {
  transition: background-color 0.5s ease;
  background-color: ${(props: ThemeProps<ThemeType>) =>
    props.theme.backgroundColor};
  color: ${(props: ThemeProps<ThemeType>) => props.theme.color};
  font-family: "Secular One", sans-serif;
  }
`;
