import {
  Container,
  Form,
  FormButton,
  FormInput,
  MainContainer,
} from "./styled";
import { FieldValues, useForm } from "react-hook-form";
import { generatePath, useNavigate } from "react-router-dom";
import FrontEndPoints from "../../router/FrontEndPoints";
import { useMemo } from "react";
import { customAlphabet } from "nanoid";
import { useGameStore } from "../../stores/GameStore";
import axios from "axios";
import BackEndPoints from "../../router/BackEndPoints";
import toast from "react-hot-toast";

export const Home = () => {
  const pincode = useMemo(() => {
    const nanoid = customAlphabet("1234567890", 6);
    return parseInt(nanoid());
  }, []);

  const generatePinCode = useGameStore((state) => state.generatePinCode);
  const toggleMaster = useGameStore((state) => state.toggleMaster);
  const socket = useGameStore((state) => state.socket);

  const navigate = useNavigate();

  const { register, handleSubmit } = useForm();

  const joinParty = async (data: FieldValues) => {
    try {
      await axios.post(BackEndPoints.getParty, { id: data.id });

      generatePinCode(data.id);
      navigate(generatePath(FrontEndPoints.gameId, { id: data.id }));
    } catch (_) {
      toast.error(`Room ${data.id} does not exist`);
    }
  };

  const createParty = async () => {
    try {
      await axios.post(BackEndPoints.joinParty, { id: pincode });
      socket.emit("joinParty", pincode);
      generatePinCode(pincode);
      toggleMaster(true);
      navigate(generatePath(FrontEndPoints.gameId, { id: pincode }));
    } catch (err) {
      console.log("error", err);
    }
  };

  return (
    <MainContainer>
      <Container>
        <Form onSubmit={handleSubmit(joinParty)}>
          <FormInput
            placeholder="123456789"
            type={"number"}
            {...register("id", { required: true })}
          />
          <FormButton value="Join" type="submit" submit />
          <FormButton value="Create" type="button" onClick={createParty} />
        </Form>
      </Container>
    </MainContainer>
  );
};
