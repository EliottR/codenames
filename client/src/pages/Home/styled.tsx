import styled from "styled-components";
import variables from "../../utils/variables";

interface Props {
  submit?: boolean;
}

export const MainContainer = styled.div`
  color: white;
  width: 100vw;
  height: 100vh;
  position: absolute;
  top: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: -1;
`;

export const Container = styled.div`
  color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 2rem;
  border-radius: 0.5rem;
  background-color: ${variables.white};
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 2rem;

  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  input[type="number"] {
    -moz-appearance: textfield;
  }
`;

export const FormInput = styled.input`
  width: 100%;
  padding: 1rem;
  box-sizing: border-box;
  ::placeholder {
    color: #cfcfcf;
  }
`;

export const FormButton = styled.input<Props>`
  background-color: ${({ submit }) =>
    submit ? variables.red : variables.yellow};
  outline: none;
  border: none;
  color: ${({ submit }) => (submit ? "white" : "black")};
  font-weight: 600;
  font-size: 1rem;
  border-radius: 0.25rem;
  padding: 1rem;
  transition: 0.25s ease;
  width: 100%;
  cursor: pointer;

  &:hover {
    background-color: ${({ submit }) =>
      submit ? variables.yellow : variables.red};
    color: ${({ submit }) => (submit ? "black" : "white")};
  }
`;
