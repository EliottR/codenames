import styled from "styled-components";

export const CardsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(5, 12rem);
  grid-column-gap: 1rem;
  grid-row-gap: 1rem;
  margin-bottom: 1rem;
  border-radius: 0.5rem;
  padding: 1rem;
  overflow: scroll;
  justify-content: center;

  @media (max-width: 1050px) {
    justify-content: flex-start;
  }
`;

export const InfosContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;
