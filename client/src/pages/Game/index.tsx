import { CardsContainer, InfosContainer } from "./styled";
import { useCallback, useEffect, useMemo, useState } from "react";
import {
  CardType,
  PatternType,
  useGameStore,
  UserType,
} from "../../stores/GameStore";
import { Card } from "../../components/Card";
import data from "../../data/data.json";
import { Pattern } from "../../components/Pattern";
import { Code } from "../../components/Code";
import { generatePath, useLocation, useNavigate } from "react-router-dom";
import axios from "axios";
import BackEndPoints from "../../router/BackEndPoints";
import { GameModal } from "../../components/Modals/GameModal";
import Cookies from "js-cookie";
import FrontEndPoints from "../../router/FrontEndPoints";
import { toast } from "react-hot-toast";
import { UsersList } from "../../components/UsersList";
import { EndGame } from "../../components/EndGame";

export const Game = () => {
  // STORE
  const cards = useGameStore((state) => state.cards);
  const updateCards = useGameStore((state) => state.updateCards);
  const displayPattern = useGameStore((state) => state.displayPattern);
  const generatePinCode = useGameStore((state) => state.generatePinCode);
  const toggleMaster = useGameStore((state) => state.toggleMaster);
  const isMaster = useGameStore((state) => state.master);
  const spy = useGameStore((state) => state.spy);
  const socket = useGameStore((state) => state.socket);
  const username = useGameStore((state) => state.username);
  const updateUsername = useGameStore((state) => state.updateUsername);
  const toggleWaitingUser = useGameStore((state) => state.toggleWaitingUser);
  const toggleSpy = useGameStore((state) => state.toggleSpy);
  const setBegin = useGameStore((state) => state.setBegin);
  const toggleTurn = useGameStore((state) => state.toggleTurn);
  const team = useGameStore((state) => state.team);
  const updateTeam = useGameStore((state) => state.updateTeam);
  const users = useGameStore((state) => state.users);
  const updateUsers = useGameStore((state) => state.updateUsers);
  const setSpyBlue = useGameStore((state) => state.setSpyBlue);
  const setSpyRed = useGameStore((state) => state.setSpyRed);
  const whoWins = useGameStore((state) => state.whoWins);
  const setWins = useGameStore((state) => state.setWins);

  // USEMEMO
  const keycards: number[] = useMemo(() => [], []);
  const saveCards: CardType[] = useMemo(() => [], []);
  const typecards: { type: PatternType; nb: number }[] = useMemo(
    () => [
      { type: "blue", nb: 8 },
      { type: "red", nb: 8 },
      { type: "neutral", nb: 9 },
      { type: "death", nb: 1 },
    ],
    []
  );

  // STATES
  const [hasCookie, setHasCookie] = useState<boolean>(false);
  const [isWaitingUser, setWaitingUser] = useState<boolean>(true);
  const navigate = useNavigate();

  // VARIABLES
  const location = useLocation().pathname.split("/");
  const pincode = location[2];

  // FUNCTIONS
  const getRandomType = useCallback(() => {
    let random = Math.floor(Math.random() * (typecards.length - 1));

    while (
      (random === 0 && typecards[0].nb === 0) ||
      (random === 1 && typecards[1].nb === 0) ||
      (random === 2 && typecards[2].nb === 0)
    ) {
      random = Math.floor(Math.random() * (typecards.length - 1));
    }

    switch (true) {
      case random === 0:
        typecards[0].nb = typecards[0].nb - 1;
        return "blue";

      case random === 1:
        typecards[1].nb = typecards[1].nb - 1;
        return "red";

      case random === 2:
        typecards[2].nb = typecards[2].nb - 1;
        return "neutral";

      default:
        typecards[2].nb = typecards[2].nb - 1;
        return "neutral";
    }
  }, [typecards]);

  const getRandomCard = useCallback(() => {
    let random = Math.floor(Math.random() * data.words.length);

    while (keycards.indexOf(random) !== -1) {
      random = Math.floor(Math.random() * data.words.length);
    }

    keycards.push(random);

    const randomType = getRandomType();
    saveCards.push({
      word: data.words[random],
      checked: false,
      type: randomType,
    });
  }, [saveCards, getRandomType, keycards]);

  const getRandomDeath = useCallback(() => {
    const neutralCards: number[] = [];

    saveCards.forEach((word, key) => {
      if (word.type === "neutral") {
        neutralCards.push(key);
      }
    });

    const random = Math.round(Math.random() * neutralCards.length);

    saveCards.splice(neutralCards[random], 1, {
      ...saveCards[neutralCards[random]],
      type: "death",
    });
  }, [saveCards]);

  const getRandomBegin = useCallback(() => {
    const random = Math.round(Math.random());

    if (random === 1) {
      setBegin("red");
      toggleTurn("red");
      return "red";
    } else {
      setBegin("blue");
      toggleTurn("blue");

      return "blue";
    }
  }, [setBegin, toggleTurn]);

  const saveDataParty = useCallback(async () => {
    updateCards(saveCards);
    const randomBegin = getRandomBegin();

    try {
      await axios.post(BackEndPoints.saveDataParty, {
        PartyId: pincode,
        cards: JSON.stringify(saveCards),
        begin: randomBegin,
        turn: randomBegin,
      });
    } catch (err) {
      console.log("err", err);
    }
  }, [saveCards, pincode, getRandomBegin, updateCards]);

  const getDataParty = useCallback(async () => {
    const getCards: CardType[] = [];

    try {
      const resp = await axios.post(BackEndPoints.getParty, { id: pincode });

      const respDataParty = resp.data.dataParty;
      const respUsers: UserType[] = resp.data.users;

      const spyRed: UserType | undefined = respUsers.find(
        (user) => user.spy === true && user.team === "red"
      );
      const spyBlue: UserType | undefined = respUsers.find(
        (user) => user.spy === true && user.team === "blue"
      );

      spyRed && setSpyRed(spyRed.username);
      spyBlue && setSpyBlue(spyBlue.username);

      updateUsers(respUsers);
      setBegin(respDataParty.begin);
      toggleTurn(respDataParty.turn);
      setWins(respDataParty.whoWins);

      JSON.parse(respDataParty.cards).forEach((card: CardType) => {
        getCards.push(card);
      });

      updateCards(getCards);
    } catch (_) {
      navigate(generatePath(FrontEndPoints.home));
      toast.error(`Room ${pincode} does not exist`);
    }
  }, [
    pincode,
    setBegin,
    navigate,
    updateCards,
    updateUsers,
    setSpyBlue,
    setSpyRed,
    toggleTurn,
    setWins,
  ]);

  const generateJwt = useCallback(async () => {
    try {
      const resp = await axios.post(BackEndPoints.generateJwt, {
        PartyId: pincode,
        username: username,
        spy: spy,
        team: team,
      });

      Cookies.set(`tkn_${pincode}`, resp.data.token, {
        expires: 1,
        sameSite: "None",
        secure: true,
      });
    } catch (err) {
      console.log("error", err);
    }
  }, [pincode, username, spy, team]);

  const decodeJwt = useCallback(async () => {
    const token = Cookies.get(`tkn_${pincode}`);

    if (token) {
      setHasCookie(true);
      try {
        const resp = await axios.post(BackEndPoints.decodeJwt, {
          token: token,
        });

        if (
          resp.data.username &&
          resp.data.PartyId === pincode &&
          resp.data.team
        ) {
          updateUsername(resp.data.username);
          toggleSpy(resp.data.spy);
          updateTeam(resp.data.team);
          setWaitingUser(false);
          generatePinCode(parseInt(pincode));
        }
      } catch (err) {
        console.log("error", err);
      }
    }
  }, [pincode, updateUsername, generatePinCode, updateTeam, toggleSpy]);

  const getData = useCallback(async () => {
    await decodeJwt();
    await getDataParty();
  }, [decodeJwt, getDataParty]);

  //SOCKET.IO
  useEffect(() => {
    socket.emit("joinParty", pincode);
    socket.on("updateDataParty", () => {
      getDataParty();
    });

    return () => {
      socket.off("connect");
      socket.off("disconnect");
      socket.off("joinParty");
      socket.off(pincode);
      updateUsers([]);
      updateTeam(null);
      updateCards([]);
      toggleMaster(false);
      setBegin("");
      toggleTurn("");
      setWaitingUser(false);
      updateUsername("");
      toggleSpy(false);
      setSpyBlue("");
      setSpyRed("");
      setWins("");
    };
  }, [
    socket,
    pincode,
    toggleMaster,
    toggleSpy,
    getDataParty,
    updateCards,
    updateTeam,
    updateUsers,
    setBegin,
    updateUsername,
    setSpyBlue,
    setSpyRed,
    toggleTurn,
    setWins,
  ]);

  useEffect(() => {
    if (isMaster) {
      for (let i = 0; i < 25; i++) {
        getRandomCard();
      }

      getRandomDeath();
      saveDataParty();
    } else {
      getData();
    }

    return () => {
      generatePinCode(null);
    };
  }, [
    isMaster,
    getRandomCard,
    getRandomDeath,
    saveDataParty,
    generatePinCode,
    getData,
  ]);

  useEffect(() => {
    if (pincode && username && !hasCookie) {
      generateJwt();
      toggleWaitingUser(isWaitingUser);
    }
  }, [
    isMaster,
    pincode,
    username,
    generateJwt,
    toggleWaitingUser,
    isWaitingUser,
    hasCookie,
  ]);

  // RENDER
  const renderCards = useMemo(() => {
    return cards.map((card) => {
      return (
        <Card
          word={card.word}
          type={card.type}
          checked={card.checked}
          key={card.word}
        />
      );
    });
  }, [cards]);

  return (
    <>
      <InfosContainer>
        <UsersList users={users} team="red" />
        <Code code={parseInt(pincode)} />
        <UsersList users={users} team="blue" />
      </InfosContainer>
      {isWaitingUser ? (
        <GameModal displayModal={() => setWaitingUser(false)} />
      ) : (
        <CardsContainer>{renderCards}</CardsContainer>
      )}

      <Pattern isVisible={displayPattern} />
      {whoWins && <EndGame whoWins={whoWins} />}
    </>
  );
};
