const variables = {
    black: "#0D0D0D",
    red: "#A62508",
    blue: "#325A9A",
    white: "#F2F2F2",
    yellow: "#F2B705" 
}

export default variables