import styled from "styled-components";

export const MainContainer = styled.p`
  font-size: 3rem;
  display: flex;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  align-items: center;
  justify-content: center;
  position: absolute;
  border: 3px solid black;

  p {
    color: red;
    border-radius: 1px solid black;
    text-shadow: 2px 0 #000, -2px 0 #000, 0 2px #000, 0 -2px #000, 1px 1px #000,
      -1px -1px #000, 1px -1px #000, -1px 1px #000;
  }
`;
