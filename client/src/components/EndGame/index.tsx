import React from "react";
import { useGameStore } from "../../stores/GameStore";
import { MainContainer } from "./styled";

type Props = {
  whoWins: "red" | "blue";
};

export const EndGame = ({ whoWins }: Props) => {
  const team = useGameStore((state) => state.team);

  if (team === whoWins) {
    return (
      <MainContainer>
        <p>YOU WIN</p>
      </MainContainer>
    );
  } else {
    return (
      <MainContainer>
        <p>YOU LOSE</p>
      </MainContainer>
    );
  }
};
