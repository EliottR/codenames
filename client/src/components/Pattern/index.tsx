import { AnimatePresence } from "framer-motion";
import { useCallback } from "react";
import { useGameStore } from "../../stores/GameStore";
import { Backdrop, Begin, MainContainer, Place } from "./styled";

type Props = {
  isVisible: boolean;
};
export const Pattern = ({ isVisible }: Props) => {
  const cards = useGameStore((state) => state.cards);
  const togglePattern = useGameStore((state) => state.togglePattern);
  const whoBegin = useGameStore((state) => state.whoBegin);

  const renderData = useCallback(() => {
    return cards.map(({ type }, index) => {
      return <Place type={type} key={index} />;
    });
  }, [cards]);

  return (
    <AnimatePresence>
      {isVisible && (
        <Backdrop
          key="pattern"
          onClick={() => togglePattern()}
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{ duration: 0.25, type: "tween" }}
        >
          <MainContainer>
            <Begin color={whoBegin}>{renderData()}</Begin>
          </MainContainer>
        </Backdrop>
      )}
    </AnimatePresence>
  );
};
