import { motion } from "framer-motion";
import styled from "styled-components";

interface PlaceProps {
  type: "red" | "blue" | "neutral" | "death";
}

interface BeginProps {
  color: "" | "red" | "blue";
}

export const Backdrop = styled(motion.div)`
  width: 100vw;
  height: 100vh;
  position: absolute;
  top: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 1;
  background-color: #00000077;
`;

export const MainContainer = styled.div`
  width: 12.5rem;
  height: 12.5rem;
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  grid-column-gap: 0.25rem;
  grid-row-gap: 0.25rem;
  background-color: #91776e;
  border-radius: 0.5rem;
`;

export const Begin = styled.div<BeginProps>`
  all: inherit;

  border: ${({ color }) => {
    switch (true) {
      case color === "red":
        return "0.5rem dashed red";

      case color === "blue":
        return "0.5rem dashed blue";

      default:
        break;
    }
  }};
`;

export const Place = styled.div<PlaceProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${({ type }) => getColor(type)};
`;

const getColor = (type: string) => {
  switch (true) {
    case type === "red":
      return "red";

    case type === "blue":
      return "blue";

    case type === "neutral":
      return "#d4c9bd";

    case type === "death":
      return "black";

    default:
      return "green";
  }
};
