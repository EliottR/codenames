import { useLocation } from "react-router-dom";
import { HomeBtn } from "../Buttons/Home";
import { SwitchTheme } from "../Buttons/SwitchTheme";
import { Score } from "../Score";
import { MainContainer, RightContainer } from "./styled";
import FrontEndPoints from "../../router/FrontEndPoints";
import { Pattern } from "../Buttons/Pattern";

interface Props {
  theme: boolean;
}

export const Header = ({ theme }: Props) => {
  const location = useLocation().pathname.split("/");
  const pathname = location[1];

  return (
    <MainContainer>
      <HomeBtn />
      {pathname === FrontEndPoints.game && <Score />}
      <RightContainer>
        {pathname === FrontEndPoints.game && <Pattern theme={theme} />}
        <SwitchTheme theme={theme} />
      </RightContainer>
    </MainContainer>
  );
};
