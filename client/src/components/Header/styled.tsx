import styled from "styled-components";

export const MainContainer = styled.div`
  margin: 1rem;
  display: flex;
  justify-content: space-between;
  position: relative;
  z-index: 2;
`;

export const RightContainer = styled.div`
  display: flex;
  gap: 0.5rem;
`;
