import { useGameStore } from "../../stores/GameStore";
import { useThemeStore } from "../../stores/ThemeStore";
import { MainContainer, TextContent } from "./styled";

type Props = {
  code: number;
};

export const Code = ({ code }: Props) => {
  const theme = useThemeStore((state) => state.theme);
  const turn = useGameStore((state) => state.turn);
  const username = useGameStore((state) => state.username);

  return (
    <MainContainer dark={theme}>
      <TextContent>
        PIN code : <span>{code}</span>
      </TextContent>
      {username && <TextContent>turn : {turn}</TextContent>}
    </MainContainer>
  );
};
