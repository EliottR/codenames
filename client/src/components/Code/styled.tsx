import styled from "styled-components";

interface Props {
  dark?: boolean;
}

export const MainContainer = styled.div<Props>`
  width: fit-content;
  margin: 0 auto;
  margin-bottom: 1rem;
  padding: 0.25rem 0.5rem;
  position: absolute;
  left: 0;
  right: 0;

  span {
    font-size: 1.25rem;
    color: ${({ dark }) => (dark ? "black" : "white")};
  }
`;

export const TextContent = styled.p`
  text-align: center;
`;
