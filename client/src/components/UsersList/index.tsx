import { useMemo } from "react";
import { useGameStore } from "../../stores/GameStore";
import { MainContainer } from "./styled";

type Props = {
  users: {
    username: string;
    PartyId: number;
    spy: boolean;
    team: "red" | "blue";
  }[];
  team: "red" | "blue";
};

export const UsersList = ({ users, team }: Props) => {
  const username = useGameStore((state) => state.username);
  const spyRed = useGameStore((state) => state.spyRed);
  const spyBlue = useGameStore((state) => state.spyBlue);

  const render = useMemo(() => {
    return users
      .filter((user) => {
        return user.team === team;
      })
      .map((user) => {
        return (
          <li key={user.username}>
            {user.username} {user.username === username && "(me)"}{" "}
            {(user.username === spyRed || user.username === spyBlue) && "- SPY"}
          </li>
        );
      });
  }, [users, team, username, spyBlue, spyRed]);
  return <MainContainer team={team}>{render}</MainContainer>;
};
