import styled from "styled-components";
import variables from "../../utils/variables";

interface Props {
  team?: "red" | "blue";
}

export const MainContainer = styled.ul<Props>`
  width: fit-content;
  margin: 0 auto;
  margin-bottom: 1rem;
  color: ${({ team }) => (team === "red" ? variables.red : variables.blue)};
  padding: 0.25rem 1rem;
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  align-items: ${({ team }) => (team === "red" ? "flex-start" : "flex-end")};
  list-style: inside;

  span {
    font-size: 1.25rem;
  }
`;
