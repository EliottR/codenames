import axios from "axios";
import { AnimatePresence } from "framer-motion";
import { useState } from "react";
import { FieldValues, useForm } from "react-hook-form";
import { useLocation } from "react-router-dom";
import BackEndPoints from "../../../router/BackEndPoints";
import { useGameStore, UserType } from "../../../stores/GameStore";
import {
  Backdrop,
  Form,
  FormButton,
  FormInput,
  FormLabel,
  RadioList,
} from "./styled";

type Props = {
  displayModal: () => void;
};
export const GameModal = ({ displayModal }: Props) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UserType>();

  const [team, setTeam] = useState<"red" | "blue">("red");
  const [check, setCheck] = useState<boolean>(false);

  const socket = useGameStore((state) => state.socket);
  const updateUser = useGameStore((state) => state.updateUsers);
  const users = useGameStore((state) => state.users);
  const updateUsername = useGameStore((state) => state.updateUsername);
  const updateTeam = useGameStore((state) => state.updateTeam);
  const toggleSpy = useGameStore((state) => state.toggleSpy);
  const setSpyBlue = useGameStore((state) => state.setSpyBlue);
  const setSpyRed = useGameStore((state) => state.setSpyRed);
  const spyRed = useGameStore((state) => state.spyRed);
  const spyBlue = useGameStore((state) => state.spyBlue);

  // VARIABLES
  const location = useLocation().pathname.split("/");
  const pincode = location[2];

  const saveUser = async (data: FieldValues) => {
    const user: UserType = {
      PartyId: parseInt(pincode),
      username: data.username,
      spy: data.spy,
      team: data.team,
    };

    socket.emit("updateDataParty", pincode);

    try {
      await axios.post(BackEndPoints.addUser, user);
      updateUser([...users, user]);
      updateUsername(data.username);
      updateTeam(data.team);
      toggleSpy(data.spy);
      if (data.team === "red" && data.spy) {
        setSpyRed(data.username);
      } else if (data.team === "blue" && data.spy) {
        setSpyBlue(data.username);
      }
      displayModal();
    } catch (err) {
      console.log("error", err);
      console.log(errors);
    }
  };

  const getStateSpy = () => {
    if ((team === "blue" && spyBlue) || (team === "red" && spyRed)) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <AnimatePresence>
      <Backdrop
        key="pattern"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 0.25, type: "tween" }}
      >
        <Form onSubmit={handleSubmit(saveUser)}>
          <FormInput
            placeholder="Usename"
            type={"text"}
            {...register("username", {
              required: true,
              maxLength: 32,
            })}
          />
          <RadioList>
            <FormLabel htmlFor="red">
              <FormInput
                type={"radio"}
                value="red"
                id="red"
                color="red"
                {...register("team", {
                  onChange: (e) => {
                    setTeam(e.target.value);
                    setCheck(false);
                  },
                  required: true,
                })}
                defaultChecked
              />
              Red
            </FormLabel>
            <FormLabel htmlFor="blue">
              <FormInput
                type={"radio"}
                value="blue"
                id="blue"
                color="blue"
                {...register("team", {
                  onChange: (e) => {
                    setTeam(e.target.value);
                    setCheck(false);
                  },
                  required: true,
                })}
              />
              Blue
            </FormLabel>
          </RadioList>
          <RadioList>
            <FormLabel htmlFor="spy" disabled={getStateSpy()}>
              <FormInput
                type={"checkbox"}
                id="spy"
                {...register("spy", {
                  onChange: () => {
                    setCheck(!check);
                  },
                })}
                disabled={getStateSpy()}
                checked={check}
              />
              Spy
            </FormLabel>
          </RadioList>
          <FormButton value="Submit" type="submit" submit />
        </Form>
      </Backdrop>
    </AnimatePresence>
  );
};
