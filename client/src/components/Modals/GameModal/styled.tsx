import { motion } from "framer-motion";
import styled from "styled-components";
import variables from "../../../utils/variables";

type Props = {
  submit?: boolean;
};

type ColorProps = {
  color?: "red" | "blue";
  disabled?: boolean;
};

export const Backdrop = styled(motion.div)`
  width: 100vw;
  height: 100vh;
  position: absolute;
  top: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 1;
  background-color: #00000077;
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 0.25rem;
  background-color: #f2f2f2;
  padding: 2rem;
  border-radius: 0.5rem;

  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  input[type="text"] {
    -moz-appearance: textfield;
  }
`;

export const RadioList = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-around;
`;

export const FormInput = styled.input<ColorProps>`
  width: 100%;
  padding: 1rem;
  box-sizing: border-box;
  ::placeholder {
    color: #cfcfcf;
  }
  accent-color: ${({ color }) => {
    switch (true) {
      case color === "red":
        return variables.red;

      case color === "blue":
        return variables.blue;
      default:
        return variables.black;
    }
  }};
`;

export const FormLabel = styled.label<ColorProps>`
  padding: 1rem 2rem;
  display: flex;
  gap: 0.5rem;
  color: ${({ disabled }) => (disabled ? "#C1C1C8" : "black")};
`;

export const FormButton = styled.input<Props>`
  background-color: ${({ submit }) =>
    submit ? variables.red : variables.yellow};
  outline: none;
  border: none;
  color: ${({ submit }) => (submit ? "white" : "black")};
  font-weight: 600;
  font-size: 1rem;
  border-radius: 0.25rem;
  padding: 1rem;
  transition: 0.25s ease;
  width: 100%;
  cursor: pointer;

  &:hover {
    background-color: ${({ submit }) =>
      submit ? variables.black : variables.yellow};
    color: "white";
  }
`;
