import axios from "axios";
import JSConfetti from "js-confetti";
import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import BackEndPoints from "../../router/BackEndPoints";
import { CardType, useGameStore } from "../../stores/GameStore";
import { UnveiledCard } from "../UnveiledCard";
import {
  Borders,
  MainContainer,
  Word,
  TopContent,
  Line,
  Image,
} from "./styled";

type Props = {
  word: string;
  type: "red" | "blue" | "neutral" | "death";
  checked: boolean;
};

export const Card = ({ word, type, checked }: Props) => {
  const cards = useGameStore((state) => state.cards);
  const isSpy = useGameStore((state) => state.spy);
  const team = useGameStore((state) => state.team);
  const turn = useGameStore((state) => state.turn);
  const toggleTurn = useGameStore((state) => state.toggleTurn);
  const updateCards = useGameStore((state) => state.updateCards);
  const socket = useGameStore((state) => state.socket);
  const [reveil, setReveil] = useState<boolean>(checked);
  const [isViewed, toggleViewed] = useState<boolean>(checked);
  const setWins = useGameStore((state) => state.setWins);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const jsConfetti = new JSConfetti();

  // VARIABLES
  const location = useLocation().pathname.split("/");
  const pincode = location[2];

  const reveilCard = () => {
    setReveil(true);
    toggleViewed(!isViewed);
    handleScore();
  };

  const handleClick = () => {
    if (!isSpy && team === turn && checked === false) {
      reveilCard();
    } else if (!isSpy && checked === true) {
      toggleViewed(!isViewed);
    }
  };

  useEffect(() => {
    setReveil(checked);
    toggleViewed(checked);
  }, [checked]);

  useEffect(() => {
    if (reveil === true && isViewed === false) {
      setTimeout(() => {
        toggleViewed(!isViewed);
      }, 5000);
    }

    return () => {
      jsConfetti.clearCanvas();
    };
  }, [isViewed, reveil, jsConfetti]);

  const handleScore = async () => {
    let postTurn: "red" | "blue";
    let whoWins: "red" | "blue" | "" = "";
    const index = cards.findIndex((card) => {
      return card.word === word;
    });

    const card = cards.find((card: CardType) => {
      return card.word === word;
    });

    const changeCard = { ...card, checked: true };

    const newCards = [...cards];

    newCards.splice(index, 1, changeCard as CardType);

    updateCards(newCards);

    if (turn === "red") {
      toggleTurn("blue");
      postTurn = "blue";
    } else {
      toggleTurn("red");
      postTurn = "red";
    }

    if (type === "death") {
      if (team === "blue") {
        setWins("red");
        whoWins = "red";
      } else {
        setWins("blue");
        whoWins = "blue";
      }
    }

    await axios.post(BackEndPoints.updateDataParty, {
      PartyId: pincode,
      cards: JSON.stringify(newCards),
      turn: postTurn,
      whoWins: whoWins,
    });

    socket.emit("updateDataParty", pincode);

    if (type === "death") {
      await jsConfetti.addConfetti({
        emojis: ["😭", "💀", "😵", "☠️"],
        emojiSize: 100,
        confettiNumber: 150,
      });
    }
  };

  return (
    <article>
      <MainContainer
        onDoubleClick={handleClick}
        isSpy={isSpy}
        visible={isViewed}
      >
        <Borders>
          <TopContent>
            <Line />
            <Image checked={checked} type={type} />
          </TopContent>
          <Word>{word}</Word>
        </Borders>
      </MainContainer>
      <UnveiledCard
        visible={isViewed}
        color={type}
        onDoubleClick={handleClick}
      />
    </article>
  );
};
