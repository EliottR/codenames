import styled from "styled-components";
import variables from "../../utils/variables";

type Props = {
  checked: boolean;
  type: "red" | "blue" | "neutral" | "death";
};

type SpyProps = {
  isSpy: boolean;
  visible: boolean;
};

export const MainContainer = styled.section<SpyProps>`
  width: 12rem;
  height: 6rem;
  background-color: #f0d8bb;
  border-radius: 0.75rem;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: ${({ isSpy }) => (isSpy ? "auto" : "pointer")};
  transition: 0.25s ease;

  &:hover {
    ${({ visible }) =>
      visible
        ? ""
        : "transform: scale(0.97); box-shadow: 0 0px 12px white"}/* transform: scale(0.97);
    box-shadow: 0 0px 12px white; */
  }
`;

export const Borders = styled.span`
  border-radius: 0.5rem;
  width: 90%;
  height: 85%;
  border: 3px solid #aa977b;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  padding: 0.5rem;
`;

export const TopContent = styled.section`
  flex-grow: 1;
  display: flex;
  align-items: flex-end;
  padding-bottom: 5%;
  gap: 0.5rem;
`;

export const Word = styled.section`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: white;
  flex-grow: 0;
  border-radius: 0 0 0.25rem 0.25rem;
  text-transform: uppercase;
  font-family: "Secular One", sans-serif;
  font-size: 1.25rem;
  font-weight: bold;
  padding: 3% 0;
  color: black;
  user-select: none;
`;

export const Line = styled.span`
  flex-grow: 1;
  height: 3px;
  background-color: #aa977b;
  display: block;
`;

export const Image = styled.div<Props>`
  background-color: ${({ checked, type }) => {
    if (checked) {
      switch (true) {
        case type === "blue":
          return variables.blue;
        case type === "red":
          return variables.red;
        case type === "neutral":
          return variables.yellow;
        case type === "death":
          return variables.black;

        default:
          return "transparent";
      }
    } else {
      return "transparent";
    }
  }};
  width: 10%;
  height: 100%;
  box-sizing: border-box;
  border: 3px solid white;
  border-radius: 3px;
`;
