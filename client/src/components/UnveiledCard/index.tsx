import { AnimatePresence } from "framer-motion";
import { MainContainer } from "./styled";

type Props = {
  color: "red" | "blue" | "neutral" | "death";
  visible: boolean;
  onDoubleClick: () => void;
};
export const UnveiledCard = ({ color, visible, onDoubleClick }: Props) => {
  return (
    <AnimatePresence>
      {visible && (
        <MainContainer
          onDoubleClick={onDoubleClick}
          color={color}
          key="unveiledcards"
          initial={{ scale: 0, rotate: 370 }}
          animate={{ scale: 1, rotate: -370 }}
          exit={{ scale: 0, rotate: 370 }}
          transition={{ duration: 0.05, type: "tween" }}
        ></MainContainer>
      )}
    </AnimatePresence>
  );
};
