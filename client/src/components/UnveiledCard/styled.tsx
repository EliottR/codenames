import { motion } from "framer-motion";
import styled from "styled-components";

type Props = {
  color: "red" | "blue" | "neutral" | "death";
};
export const MainContainer = styled(motion.div)<Props>`
  position: relative;
  margin-top: -50%;
  width: 12rem;
  height: 6rem;
  background-color: #f0d8bb;
  border-radius: 0.75rem;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transition: 0.25s ease;
  background: ${({ color }) => {
    switch (true) {
      case color === "red":
        return "radial-gradient(circle, rgba(161,25,25,1) 0%, rgba(61,0,0,1) 100%)";

      case color === "blue":
        return "radial-gradient(circle,rgba(25, 42, 161, 1) 0%,rgba(0, 1, 61, 1) 100%)";

      case color === "death":
        return "radial-gradient(circle, rgba(54,54,54,1) 0%, rgba(0,0,0,1) 100%)";
      default:
        return "radial-gradient(circle, rgba(255,205,99,1) 0%, rgba(157,125,0,1) 100%)";
    }
  }};

  &:hover {
    transform: scale(0.97);
    box-shadow: 0 0px 12px white;
  }
`;
