import { MainContainer } from "./styled";
import { IoAppsOutline } from "react-icons/io5";
import { useGameStore } from "../../../stores/GameStore";

type Props = {
  theme: boolean;
};

export const Pattern = ({ theme }: Props) => {
  const togglePattern = useGameStore((state) => state.togglePattern);
  const spy = useGameStore((state) => state.spy);

  return (
    <MainContainer dark={theme} onClick={() => togglePattern()} disabled={spy}>
      <IoAppsOutline size={32} />
    </MainContainer>
  );
};
