import styled from "styled-components";

interface Props {
  dark?: boolean;
}

export const MainContainer = styled.div<Props>`
  width: 2.5rem;
  height: 2.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  border: 2px solid;
  border-color: ${({ dark }) => (dark ? "black" : "white")};
  padding: 0.25rem;
  border-radius: 0.25rem;
  transition: 0.25s ease;

  :hover {
    opacity: 0.5;
  }
`;
