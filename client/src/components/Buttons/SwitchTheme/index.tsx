import { MainContainer } from "./styled";
import { IoSunny, IoMoon } from "react-icons/io5";
import { useThemeStore } from "../../../stores/ThemeStore";

type Props = {
  theme: boolean;
};

export const SwitchTheme = ({ theme }: Props) => {
  const setTheme = useThemeStore((state) => state.setTheme);

  const handleClick = () => {
    setTheme();
    localStorage.setItem("theme", (!theme).toString());
  };

  return (
    <MainContainer onClick={handleClick} dark={theme}>
      {theme ? <IoSunny size={32} /> : <IoMoon size={32} />}
    </MainContainer>
  );
};
