import { MainContainer } from "./styled";
import { IoHome } from "react-icons/io5";
import { useThemeStore } from "../../../stores/ThemeStore";
import FrontEndPoints from "../../../router/FrontEndPoints";

export const HomeBtn = () => {
  const theme = useThemeStore((state) => state.theme);

  return (
    <MainContainer to={FrontEndPoints.home}>
      <IoHome size={32} color={theme ? "black" : "white"} />
    </MainContainer>
  );
};
