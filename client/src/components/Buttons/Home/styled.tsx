import { motion } from "framer-motion";
import { Link } from "react-router-dom";
import styled from "styled-components";

interface Props {}

export const MainContainer = styled(motion(Link))<Props>`
  width: 2.5rem;
  height: 2.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  border: 2px solid;
  padding: 0.25rem;
  border-radius: 0.25rem;
  transition: 0.25s ease;
  text-decoration: none;
  color: inherit;
  margin-right: 3.5rem;

  :hover {
    opacity: 0.5;
  }
`;
