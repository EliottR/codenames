import styled from "styled-components";
import variables from "../../utils/variables";

interface Props {
  second?: boolean;
}

type TeamProps = {
  team: "red" | "blue" | null;
};

export const MainContainer = styled.div<TeamProps>`
  display: flex;
  height: 4rem;
  width: fit-content;
  margin: 0 auto;
  gap: 1rem;
  padding: 0.25rem;
  border: ${({ team }) => {
    switch (true) {
      case team === "blue":
        return `0.25rem solid ${variables.blue}`;

      case team === "red":
        return `0.25rem solid ${variables.red}`;

      default:
        break;
    }
  }};
`;

export const Container = styled.div<Props>`
  width: 3.5rem;
  height: 100%;
  background: ${({ second }) =>
    second
      ? "linear-gradient(90deg, rgba(12,92,141,1) 0%, rgba(12,39,62,1) 100%)"
      : "linear-gradient(90deg,rgba(53, 3, 10, 1) 0%,rgba(209, 66, 39, 1) 81%,rgba(245, 122, 71, 1) 100%)"};
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 2rem;
  padding: ${({ second }) => (second ? "0 0 0 0.5rem" : "0 0.5rem 0 0")};
  clip-path: ${({ second }) =>
    second
      ? "polygon(0 0, 100% 0, 100% 100%, 25% 100%)"
      : "polygon(0 0, 100% 0, 75% 100%, 0% 100%)"};
`;

export const LogoSeparator = styled.img`
  width: 100px;
`;
