import { Container, LogoSeparator, MainContainer } from "./styled";
import logo from "../../assets/images/logo.png";
import { useGameStore } from "../../stores/GameStore";
import { useMemo } from "react";

export const Score = () => {
  const team = useGameStore((state) => state.team);
  const cards = useGameStore((state) => state.cards);

  const scoreRed = useMemo(() => {
    const score = cards.filter(
      (card) => card.checked === true && card.type === "red"
    );

    return 8 - score.length;
  }, [cards]);

  const scoreBlue = useMemo(() => {
    const score = cards.filter(
      (card) => card.checked === true && card.type === "blue"
    );

    return 8 - score.length;
  }, [cards]);

  return (
    <MainContainer team={team}>
      <Container>{scoreRed}</Container>
      <LogoSeparator src={logo} alt="" />
      <Container second>{scoreBlue}</Container>
    </MainContainer>
  );
};
