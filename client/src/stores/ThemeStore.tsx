import { create } from "zustand";

interface ThemeState {
  theme: boolean;
  setTheme: () => void;
}

const themeStorage = localStorage.getItem("theme");

export const useThemeStore = create<ThemeState>((set) => ({
  theme: themeStorage === "true" ? true : false,
  setTheme: () => set((state) => ({ theme: !state.theme })),
}));
