import { create } from "zustand";
import { mountStoreDevtool } from "simple-zustand-devtools";
import { ReactNode } from "react";
import { io, Socket } from "socket.io-client";
import { DefaultEventsMap } from "socket.io/dist/typed-events";

interface GameState {
  cards: CardType[];
  updateCards: (arr: CardType[]) => void;
  displayPattern: boolean;
  togglePattern: () => void;
  pincode: number | null;
  generatePinCode: (pincode: number | null) => void;
  master: boolean;
  toggleMaster: (bool: boolean) => void;
  spy: boolean;
  toggleSpy: (bool: boolean) => void;
  spyBlue: string;
  setSpyBlue: (string: string) => void;
  spyRed: string;
  setSpyRed: (bool: string) => void;
  waitingUser: boolean;
  toggleWaitingUser: (bool: boolean) => void;
  socket: Socket<DefaultEventsMap, DefaultEventsMap>;
  username: string | null;
  updateUsername: (username: string) => void;
  team: "red" | "blue" | null;
  updateTeam: (team: "red" | "blue" | null) => void;
  whoBegin: "red" | "blue" | "";
  setBegin: (string: "red" | "blue" | "") => void;
  users: UserType[];
  updateUsers: (arr: UserType[]) => void;
  turn: "red" | "blue" | "";
  toggleTurn: (team: "red" | "blue" | "") => void;
  whoWins: "red" | "blue" | "";
  setWins: (team: "red" | "blue" | "") => void;
}

export type PatternType = "red" | "blue" | "neutral" | "death";

export type Cards = {
  node: ReactNode;
};
export type CardType = {
  word: string;
  checked: boolean;
  type: PatternType;
};

export type UserType = {
  username: string;
  spy: boolean;
  PartyId: number;
  team: "red" | "blue";
};

const socket = io(process.env.REACT_APP_URL as string);

export const useGameStore = create<GameState>((set) => ({
  cards: [],
  updateCards: (arr: CardType[]) =>
    set((state) => ({ cards: (state.cards = arr) })),
  displayPattern: false,
  togglePattern: () =>
    set((state) => ({ displayPattern: !state.displayPattern })),
  pincode: null,
  generatePinCode: (pincode: number | null) =>
    set((state) => ({ pincode: (state.pincode = pincode) })),
  master: false,
  toggleMaster: (bool: boolean) => set(() => ({ master: bool })),
  spy: false,
  toggleSpy: (bool: boolean) => set(() => ({ spy: bool })),
  spyBlue: "",
  setSpyBlue: (string: string) => set(() => ({ spyBlue: string })),
  spyRed: "",
  setSpyRed: (string: string) => set(() => ({ spyRed: string })),
  waitingUser: false,
  toggleWaitingUser: (bool: boolean) => set(() => ({ waitingUser: bool })),
  socket: socket,
  username: null,
  updateUsername: (username: string | null) =>
    set((state) => ({ username: (state.username = username) })),
  team: null,
  updateTeam: (team: "red" | "blue" | null) =>
    set((state) => ({ team: (state.team = team) })),
  whoBegin: "",
  setBegin: (string: "red" | "blue" | "") =>
    set((state) => ({ whoBegin: (state.whoBegin = string) })),
  users: [],
  updateUsers: (arr: UserType[]) =>
    set((state) => ({ users: (state.users = arr) })),
  turn: "",
  toggleTurn: (team: "red" | "blue" | "") =>
    set((state) => ({ turn: (state.turn = team) })),
  whoWins: "",
  setWins: (team: "red" | "blue" | "") =>
    set((state) => ({ whoWins: (state.whoWins = team) })),
}));

if (process.env.NODE_ENV === "development") {
  mountStoreDevtool("GameStore", useGameStore);
}
