import { Toaster } from "react-hot-toast";
import { Header } from "./components/Header";
import { Router } from "./router";
import { useThemeStore } from "./stores/ThemeStore";
import Theme, { GlobalStyle } from "./styled";

const App = () => {
  const theme = useThemeStore((state) => state.theme);

  return (
    <Theme theme={theme}>
      <Header theme={theme} />
      <GlobalStyle />
      <Router />
      <Toaster />
    </Theme>
  );
};

export default App;
